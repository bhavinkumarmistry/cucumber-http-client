package vu.edu.au.cucumber.http.client;

import java.net.URI;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

public class APIClient {

	private static URI uri;
	private Client apiClient;
	private HttpAuthenticationFeature basicAuth;

	public APIClient() {
		System.out.println("This is menual approch of creating http client.");
	}

	public APIClient(String bashUri) {
		System.out.println("API Bash URI : "+ bashUri);
		uri = UriBuilder.fromUri(bashUri).build();
	}
	
	public APIClient(String httpProtocol, String httpHost, String httpPort, String bashPath) {

		System.out.println(
				"API Bash URI :  " + String.format("%s://%s:%s/%s", httpProtocol, httpHost, httpPort, bashPath));
		uri = UriBuilder.fromUri(String.format("%s://%s:%s/%s", httpProtocol, httpHost, httpPort, bashPath)).build();
	}

	public APIClient(String httpProtocol, String httpHost, String httpPort, String bashPath, String username,
			String password) {

		System.out.println(
				"API Bash URI :  " + String.format("%s://%s:%s/%s", httpProtocol, httpHost, httpPort, bashPath));
		uri = UriBuilder.fromUri(String.format("%s://%s:%s/%s", httpProtocol, httpHost, httpPort, bashPath)).build();
		setBasicAuthentication(username, password);
	}

	public void setBasicAuthentication(String username, String password) {

		basicAuth = HttpAuthenticationFeature.basicBuilder().credentials(username, password).build();
		System.out.println("Authentication setuped .");

	}

	public Client getHttpClient() {
		apiClient = ClientBuilder.newClient();

		if (this.basicAuth != null) {
			apiClient.register(this.basicAuth);
		}
		System.out.println("Http Client reference setuped .");
		return apiClient;
	}

	public void close() {

		if (this.apiClient == null)
			throw new RuntimeException(
					"Http Client reference is not eligible to destroy because of it's null or invalid !!!");

		this.apiClient.close();
		System.out.println("Http Client reference closed .");
	}

	public URI getBaseURI() {
		if (uri == null)
			throw new RuntimeException("Api Client is not initialised !!! ");
		return uri;
	}

	public WebTarget createRequest(Client httpClient) {

		if (httpClient == null)
			throw new RuntimeException("Http Client reference is null or invalid !!!");

		return httpClient.target(getBaseURI());
	}

	public Response get(String path, QueryParameters queryParams, Headers headers) {
		WebTarget resource = createRequest(getHttpClient()).path(path);

		processQueryParams(resource, queryParams);

		Builder request = resource.request();

		processHeaders(request, headers);

		System.out.println("Get Resource URI : " + resource.getUri() + "Resource Headers : " + headers);
		return request.accept(MediaType.APPLICATION_JSON).get();
	}

	public Response put(String path, String payload, QueryParameters queryParams, Headers headers) {

		WebTarget resource = createRequest(getHttpClient()).path(path);

		processQueryParams(resource, queryParams);

		Builder request = resource.request();

		processHeaders(request, headers);

		return request.accept(MediaType.APPLICATION_JSON).put(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE));
	}

	public Response post(String path, String payload, QueryParameters queryParams, Headers headers) {

		WebTarget resource = createRequest(getHttpClient()).path(path);

		processQueryParams(resource, queryParams);

		Builder request = resource.request();

		processHeaders(request, headers);

		return request.accept(MediaType.APPLICATION_JSON).post(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE));
	}
	
	public Response delete(String path, QueryParameters queryParams, Headers headers) {
		WebTarget resource = createRequest(getHttpClient()).path(path);

		processQueryParams(resource, queryParams);

		Builder request = resource.request();

		processHeaders(request, headers);

		return request.accept(MediaType.APPLICATION_JSON).delete();
	}

	private void processQueryParams(WebTarget resource, QueryParameters queryParams) {
		if (queryParams != null && queryParams.getQueryParameters() != null
				&& queryParams.getQueryParameters().size() != 0) {
			Set<String> keys = queryParams.getQueryParameters().keySet();
			for (String key : keys) {
				resource = resource.queryParam(key, queryParams.getQueryParameters().get(key));
			}
		}
	}

	private void processHeaders(Builder request, Headers headers) {
		if (headers != null && headers.getHeaders() != null && headers.getHeaders().size() != 0) {
			Set<String> keySet = headers.getHeaders().keySet();
			for (String key : keySet) {
				request = request.header(key, headers.getHeaders().get(key));
			}
		}
	}

	public static void main(String[] args) {

		APIClient client = new APIClient("https", "internal.api.vu.edu.au", "443","oimrestinterface-dev/api/oimService/v1", "oim", "14oim11");

		QueryParameters queryParameters = new QueryParameters();
		queryParameters.addQueryParameters("userId", "e5023984");

		Response clientResponse = client.get("roles/defaultRole", queryParameters, null);

		System.out.println(clientResponse.readEntity(String.class));
		client.close();
	}
}
