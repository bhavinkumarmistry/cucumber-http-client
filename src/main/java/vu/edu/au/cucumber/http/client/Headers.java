package vu.edu.au.cucumber.http.client;

import java.util.HashMap;
import java.util.Map;

public class Headers {

	private Map<String, Object> Headers = new HashMap<String, Object>();

	public Map<String, Object> getHeaders() {
		return Headers;
	}

	public void addHeaders(String key, Object value) {
		Headers.put(key, value);
	}

	@Override
	public String toString() {
		return String.format("QueryParameters [queryParameters=%s]", Headers);
	}
}
