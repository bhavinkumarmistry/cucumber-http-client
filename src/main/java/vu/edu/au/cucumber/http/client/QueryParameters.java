package vu.edu.au.cucumber.http.client;

import java.util.HashMap;
import java.util.Map;

public class QueryParameters {

	private Map<String, Object> queryParameters = new HashMap<String, Object>();

	public Map<String, Object> getQueryParameters() {
		return queryParameters;
	}

	public void addQueryParameters(String key, Object value) {
		queryParameters.put(key, value);
	}

	@Override
	public String toString() {
		return String.format("QueryParameters [queryParameters=%s]", queryParameters);
	}
}
